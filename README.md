## Docker-compose
  - Build and Start the container
```sh
docker-compose up
```

## Usage
### Single prediction
  1- Open `http://localhost:8080/` in a browser

  2- Fill the form with the patient values
  
  3- Predict and wait for results! 
