#!/usr/bin/env python3
"""
Flask configuration
"""

from pathlib import Path
from os import environ
import uuid

from dotenv import load_dotenv

basedir = Path(__file__).absolute()
env_file = basedir / ".env"
load_dotenv(env_file)

class Config:
    """Base config."""

    STATIC_FOLDER = "static"
    TEMPLATES_FOLDER = "templates"
    SECRET_KEY = environ.get('SECRET_KEY', uuid.uuid4().hex)
    APP_IP = environ.get("APP_IP", "127.0.0.1")
    APP_PORT = environ.get("APP_PORT", "5000")

class ProdConfig(Config):
    FLASK_ENV = "production"
    DEBUG = False
    TESTING = False
    MAX_CONTENT_PATH = 20 * 1024 * 1024

class DevConfig(Config):
    FLASK_ENV = "development"
    DEBUG = True
    TESTING = True
    MAX_CONTENT_PATH = 50 * 1024 * 1024
