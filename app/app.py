import pandas as pd
from flask import (
    Flask,
    abort,
    redirect,
    render_template,
    request,
    send_from_directory,
    url_for,
    Response,
)
from flask_wtf.csrf import CSRFProtect
from pebble import ProcessFuture


import config
from predict import predict

app = Flask(__name__)
app.config.from_object(config.DevConfig)

csrf = CSRFProtect()
csrf.init_app(app)


@app.before_request
def csrf_protect():
    if request.method == "POST":
        csrf.protect()


@app.route("/")
def index():
    return render_template("home.html")


@app.route("/", methods=["POST"])
def single_patient():
    var = [
        "age",
        "bmi",
        "egfr",
        "dbp",
        "diabete",
        "haf",
        "hap",
        "hcerebrov",
        "hmi",
        "hpad",
        "hr",
        "hta",
        "k",
        "lvef_4c",
        "na",
        "plt",
        "pp",
        "sbp",
        "sex",
        "smk",
        "trt_antiplt",
        "trt_arb",
        "trt_aspirin",
        "trt_bb",
        "trt_ccb",
        "trt_diur",
        "trt_stat",
        "wbc",
        "hb",
    ]
    binary = [
        "trt_antiplt",
        "trt_arb",
        "trt_aspirin",
        "trt_bb",
        "trt_ccb",
        "trt_diur",
        "trt_stat",
        "diabete",
        "haf",
        "hap",
        "hcerebrov",
        "hmi",
        "hpad",
        "hta",
    ]
    val = {}
    for col in var:
        if col in request.form:
            val[col] = request.form[col]
        else:
            if col in binary:
                val[col] = "FALSE"
            else:
                val[col] = "NA"

    df = pd.DataFrame(data=[val], columns=var)
    predictions = predict(df, ",", "NA", "random_forest_model.model", True)
    predictions = predictions.result()
    if predictions == 1:
        predicted = " Older, AF, lower LVEF, lower eGFR"
        un_an = {
            "HF hospitalization or All cause death": "21% (19% - 22%)",
            "HF hospitalization or cardiovascular death": "19% (17%-21%)",
            "Cardiovascular hospitalization or All cause death": "54% (51% - 56%)",
            "MACE": "37% (35% - 39%)",
        }
        deux_ans = {
            "HF hospitalization or All cause death": "28% (26% - 30%)",
            "HF hospitalization or cardiovascular death": "26% (23% - 28%)",
            "Cardiovascular hospitalization or All cause death": "63% (60% - 65%)",
            "MACE": "46% (44% - 49%)",
        }
    elif predictions == 2:
        predicted = " Older, hypertensive, higher PP and LVEF"
        un_an = {
            "HF hospitalization or All cause death": "12% (10% - 14%)",
            "HF hospitalization or cardiovascular death": "11% (9% - 12%)",
            "Cardiovascular hospitalization or All cause death": "41% (38% - 44%)",
            "MACE": "22% (20% - 24%)",
        }
        deux_ans = {
            "HF hospitalization or All cause death": "17% (15% - 20%)",
            "HF hospitalization or cardiovascular death": "15% (12% - 17%)",
            "Cardiovascular hospitalization or All cause death": "50% (46% - 54%)",
            "MACE": "30% (27% - 34%)",
        }
    elif predictions == 3:
        predicted = " Higher BMI, hypertensive, diabetic, higher WBC and PP"
        un_an = {
            "HF hospitalization or All cause death": "16% (14% - 18%)",
            "HF hospitalization or cardiovascular death": "13% (11% - 15%)",
            "Cardiovascular hospitalization or All cause death": "50% (46% - 53%)",
            "MACE": "30% (27% - 33%)",
        }
        deux_ans = {
            "HF hospitalization or All cause death": "22% (19% - 24%)",
            "HF hospitalization or cardiovascular death": "18% (15% - 21%)",
            "Cardiovascular hospitalization or All cause death": "59% (55% - 63%)",
            "MACE": "38% (34% - 41%)",
        }
    elif predictions == 4:
        predicted = "Younger, high eGFR and LVEF"
        un_an = {
            "HF hospitalization or All cause death": "6% (5% - 7%)",
            "HF hospitalization or cardiovascular death": "5% (4% - 6%)",
            "Cardiovascular hospitalization or All cause death": "34% (31% - 36%)",
            "MACE": "13% (11% - 14%)",
        }
        deux_ans = {
            "HF hospitalization or All cause death": "9.5% (8% - 11%)",
            "HF hospitalization or cardiovascular death": "8% (6% - 9%)",
            "Cardiovascular hospitalization or All cause death": "41% (38% - 44%)",
            "MACE": "18% (16% - 21%)",
        }
    elif predictions == 5:
        predicted = "Younger, high eGFR, lower PP and BP"
        un_an = {
            "HF hospitalization or All cause death": "9% (8% - 10%)",
            "HF hospitalization or cardiovascular death": "8% (7% - 9%)",
            "Cardiovascular hospitalization or All cause death": "43% (40% - 45%)",
            "MACE": "18% (16% - 20%)",
        }
        deux_ans = {
            "HF hospitalization or All cause death": " (14% (12% - 16%)",
            "HF hospitalization or cardiovascular death": "12% (10% - 14%)",
            "Cardiovascular hospitalization or All cause death": "50% (47% - 54%)",
            "MACE": "24% (21% - 26%)",
        }

    return render_template(
        "result.html",
        content=predicted,
        idn=predictions,
        event1a=un_an,
        event2a=deux_ans,
    )


if __name__ == "__main__":
    app.run(host='0.0.0.0')
