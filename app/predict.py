#! /usr/bin/env python3

import argparse
import random
import string
from os import path
from pathlib import Path

import pandas as pd
from pebble import concurrent
from weka.classifiers import Classifier
from weka.core import jvm
from weka.core.converters import Loader
from weka.core.dataset import Attribute, Instance, Instances
from weka.filters import Filter
from werkzeug.utils import secure_filename


def get_random_string(length):
    letters = string.ascii_letters
    return "".join(random.choice(letters) for i in range(length))


@concurrent.process
def predict(
    inpt: pd.DataFrame, separator: str, missing: str, inpt_model: str, single: bool
):
    # start the jvm
    jvm.start()

    att_list = [
        "age",
        "bmi",
        "egfr",
        "dbp",
        "diabete",
        "haf",
        "hap",
        "hcerebrov",
        "hmi",
        "hpad",
        "hr",
        "hta",
        "k",
        "lvef_4c",
        "na",
        "plt",
        "pp",
        "sbp",
        "sex",
        "smk",
        "trt_antiplt",
        "trt_arb",
        "trt_aspirin",
        "trt_bb",
        "trt_ccb",
        "trt_diur",
        "trt_stat",
        "wbc",
        "hb",
    ]

    binary = [
        "trt_antiplt",
        "trt_arb",
        "trt_aspirin",
        "trt_bb",
        "trt_ccb",
        "trt_diur",
        "trt_stat",
        "diabete",
        "haf",
        "hap",
        "hcerebrov",
        "hmi",
        "hpad",
        "hta",
    ]

    weka_att = []
    values = []
    bin = {
        "FALSE": 0,
        "TRUE": 1,
    }
    sex = {
        "female": 0,
        "male": 1,
    }
    smk = {
        "smoker": 0,
        "ex-smoker": 1,
        "non-smoker": 2,
    }
    for att in att_list:
        # create attributes
        if att in binary:
            weka_att.append(Attribute.create_nominal(att, ["FALSE", "TRUE"]))
        elif att == "sex":
            weka_att.append(Attribute.create_nominal(att, ["female", "male"]))
        elif att == "smk":
            weka_att.append(
                Attribute.create_nominal(att, ["smoker", "ex-smoker", "non-smoker"])
            )
        else:
            weka_att.append(Attribute.create_numeric(att))

        if inpt[att].iloc[0] in ["", "NA"]:
            values.append(Instance.missing_value())
        elif att in binary:
            values.append(bin[inpt[att].iloc[0]])
        elif att == "sex":
            values.append(sex[inpt[att].iloc[0]])
        elif att == "smk":
            values.append(smk[inpt[att].iloc[0]])
        else:
            values.append(float(inpt[att].iloc[0]))

    # create dataset
    data = Instances.create_instances("data", weka_att, 1)
    inst = Instance.create_instance(values)
    data.add_instance(inst)

    # add cluster column for storing prediction
    add_cluster = Filter(
        classname="weka.filters.unsupervised.attribute.Add",
        options=["-T", "NOM", "-N", "cluster", "-L", "1,2,3,4,5", "-C", "last"],
    )
    add_cluster.inputformat(data)
    data = add_cluster.filter(data)
    data.class_is_last()

    # load model
    cls = Classifier(
        classname="weka.classifiers.misc.InputMappedClassifier",
        options=["-I", "-L", inpt_model, "-M"],
    )

    # predict!
    predictions = []
    for index, inst in enumerate(data):
        pred = cls.classify_instance(inst)
        predictions.append(int(pred) + 1)


    # stop the jvm
    jvm.stop()

    if single:
        return predictions[0]

