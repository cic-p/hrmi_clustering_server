# Utilisation de l'image Miniconda3 comme image de base
FROM docker.io/continuumio/miniconda3:23.10.0-1

# Installation d'OpenJDK 11 à partir des dépôts officiels Debian
RUN apt-get update && apt-get install -y openjdk-11-jdk

# Création d'un nouvel utilisateur non-root
RUN useradd -m -U flask_user

# Création du répertoire de travail dans le conteneur
WORKDIR /app
# Copie des fichiers de l'application Flask dans le conteneur pour le nouvel utilisateur
COPY . /app

# Changer le propriétaire du répertoire de travail au nouvel utilisateur
RUN chown -R flask_user:flask_user /app
RUN chown -R flask_user:flask_user /opt/conda

# Définition de l'utilisateur non-root en tant qu'utilisateur courant pour l'exécution
USER flask_user

# Copie du fichier environment.yml dans le conteneur
COPY environment.yml .

# Installation des dépendances Conda et pip pour le nouvel utilisateur
RUN conda update -y -n base conda
RUN conda install -y -n base conda-libmamba-solver python=3.10.4
RUN conda config --set solver libmamba
RUN conda env update -n base -f environment.yml && \
    conda clean --all --yes

RUN pip install python-dotenv==1.0.1 gunicorn==21.2.0 packaging==21.3 pyparsing==3.0.8 python-weka-wrapper3==0.2.9


# Exposition du port 8473 pour Gunicorn (vous pouvez utiliser un autre port si nécessaire)
EXPOSE 8473


# Commande pour démarrer l'application Flask avec Gunicorn
CMD ["gunicorn", "--bind", "0.0.0.0:8473", "app:app"]
